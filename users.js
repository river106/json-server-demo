let Mock = require('mockjs');
let random = Mock.Random;

module.exports = () => {
    let data = {
        users: []
    };

    for (let i = 1; i <= 5; i++) {
        let content = random.cparagraph(1, 5);
        data.users.push({
            id: i,
            name: random.cname(),
            phone: random.integer(11, 11),
            desc: content,
            tag: random.cword(2, 6),
            age: random.integer(18, 60),
            image: random.image('200x100', random.color(), random.word(2, 6))
        })
    }
    return data
}