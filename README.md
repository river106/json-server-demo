# 利用json-server搭建本地的数据接口

## 安装json-server
json-server: https://www.npmjs.com/package/json-server

创建文件夹json-server-demo(不要使用json-server)，初始化package,json文件
```
npm init --yes
```

安装json-server
```
npm i json-server --save
```

打开package,json，修改scripts内容为：
```
"dev": "json-server --watch db.json"
```

根目录下创建db.json,加入测试内容:
```
{
  "users": [
    {
      "id": 1,
      "name": "jason",
      "phone": "17700991122",
      "age": "31"
    },
    {
      "id": 2,
      "name": "river",
      "phone": "15600225566",
      "age": "28"
    },
    {
      "id": 3,
      "name": "lucy",
      "phone": "13155889966",
      "age": "29"
    },
    {
      "id": "4",
      "name": "grance",
      "phome": "13355669988",
      "age": "22"
    }
  ]
}
```

开启json-server
```
npm run dev
```

浏览器输入：http://localhost:3000/users 即可获取json数据。

## 与mockjs配合模拟数据
安装mockjs
```
npm install mockjs --save
```

创建users.js，内容如下：
```
let Mock = require('mockjs');
let random = Mock.Random;

module.exports = () => {
    let data = {
        users: []
    };

    for (let i = 1; i <= 5; i++) {
        let content = random.cparagraph(1, 5);
        data.users.push({
            id: i,
            name: random.cname(),
            phone: random.integer(11, 11),
            desc: content,
            tag: random.cword(2, 6),
            age: random.integer(18, 60),
            image: random.image('200x100', random.color(), random.word(2, 6))
        })
    }
    return data
}
```

在package.json的scripts下新增如下脚本：
```
"mock": "json-server --watch users.js"
```

启动json-server：
```
npm run mock
```

浏览器访问：http://localhost:3000/users，mock数据如下：
```
[
  {
    "id": 1,
    "name": "薛艳",
    "phone": 11,
    "desc": "好置增京第马参中名资只史大采进。太从商称把也条最正时数具开办。目形层特也月又动即率车京信。却且光来局八米是党世为三至。角这系接报达任低素来是究压力那情华团。",
    "tag": "资布南受火命",
    "age": 29,
    "image": "http://dummyimage.com/200x100/d279f2&text=snjl"
  },
  {
    "id": 2,
    "name": "雷秀英",
    "phone": 11,
    "desc": "切专确下别主几利族听素九条几方老片。总业机间空代候己统步参各名比。",
    "tag": "为气",
    "age": 20,
    "image": "http://dummyimage.com/200x100/79f2ae&text=fktr"
  },
  {
    "id": 3,
    "name": "曾伟",
    "phone": 11,
    "desc": "并下里高却得形生较进法候东教精快。常育图前等王只相省革力为装究么万。七心三与数西称料下民华根。成参但部毛议叫据半天感通传整育立。万术制理发但省构改决委表化儿经。",
    "tag": "王所想龙府",
    "age": 59,
    "image": "http://dummyimage.com/200x100/f28b79&text=zih"
  },
  {
    "id": 4,
    "name": "卢明",
    "phone": 11,
    "desc": "矿教资么自比思今非导青形无们土。书族二式小外阶矿体果身问包其情红当。设场地资中越何点角料论离照商业名其将。市布七经设是电系安段名拉很没族。",
    "tag": "压空",
    "age": 41,
    "image": "http://dummyimage.com/200x100/7989f2&text=tfsg"
  },
  {
    "id": 5,
    "name": "周丽",
    "phone": 11,
    "desc": "也每小增眼济理百内书山数持者油。近单率利见候上保活教细其又这用科入。系展质更能流例消点亲却快们厂。",
    "tag": "改要指矿",
    "age": 53,
    "image": "http://dummyimage.com/200x100/adf279&text=cew"
  }
]
```